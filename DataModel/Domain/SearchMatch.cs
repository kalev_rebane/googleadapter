﻿using System.IO;
using System.Linq;
using Model.Google;

namespace Model.Domain
{
    public class SearchMatch
    {
        public string Name { get; set; }
        public Candidates Reply { get; set; }


        public bool IsValid => Reply.status == "OK"
                               &&
                               (Reply.candidates.Any(x => x.name.Contains(Name)));


        public override string ToString()
        {
            using (var sw = new StringWriter())
            {
                sw.WriteLine($"{Name}  {Reply.status}");
                foreach (var c in Reply.candidates)
                    sw.WriteLine($". {c.name}   -   {c.formatted_address}");
                sw.WriteLine();
                return sw.ToString();
            }


        }
    }
}
