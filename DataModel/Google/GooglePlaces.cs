﻿using System.Collections.Generic;

namespace Model.Google
{
    
    public class Candidate
    {
        public string formatted_address { get; set; }
        public string name { get; set; }
    }

    public class Candidates
    {
        public List<Candidate> candidates { get; set; }
        public string status { get; set; }
    }


}
