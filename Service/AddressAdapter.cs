﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using Model.Domain;
using Model.Google;
using Newtonsoft.Json;

namespace GoogleAdapter.Service
{
    public class AddressAdapter
    {

        public AddressAdapter()
        {
            urlTmpl = $"https{urlTmpl}{ConfigurationManager.AppSettings["googleKey"]}";
            CompaniesWithMatches = new List<SearchMatch>();
        }


        public List<SearchMatch> CompaniesWithMatches { get; }

        string urlTmpl = "://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=####&inputtype=textquery&fields=formatted_address,name&key=";

        public SearchMatch Search( string name )
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(name);

            var json = DownloadString(urlTmpl.Replace("####", name));
            var googleMatches =  JsonConvert.DeserializeObject<Candidates>(json);


            var match = new SearchMatch()
            {
                Name = name,
                Reply = googleMatches
            };

            CompaniesWithMatches.Add(match);

            return match;

        }


        public void RemoveInvalidReplies()
        {
            foreach (var c in CompaniesWithMatches.Where(x => !x.IsValid).ToArray())
                CompaniesWithMatches.Remove(c);
        }


        public string AllResultsToJson()
        {

            if (CompaniesWithMatches.Any())
                return JsonConvert.SerializeObject(CompaniesWithMatches);

            return null;

        }

        string DownloadString(string url)
        {
            using (var wc = new WebClient() {Encoding = Encoding.UTF8 })
            {
                return wc.DownloadString(url);
            }
        }


        
    }
}
