﻿using System;
using System.IO;
using System.Linq;
using GoogleAdapter.Service;

namespace GoogleAdapter
{
    class Program
    {
        

        static void Main(string[] args)
        {

            var serv = new AddressAdapter();


            try
            {
                while (true)
                {
                    if (File.Exists("readme.md"))
                        Console.Write(File.ReadAllText("readme.md"));

                    var cmd = Console.ReadKey().KeyChar;
                    Console.WriteLine();

                    switch (cmd)
                    {
                        case '1':
                            Console.WriteLine("enter company title:");
                            var title = Console.ReadLine();
                            var company = serv.Search(title);
                            Console.WriteLine(company != null ? company.ToString() : "no result!");

                            break;

                        case '2':
                            Console.WriteLine("wait please..");
                            var companies = File.ReadAllLines("in_titles.txt");
                            foreach (var name in companies.Where(x => !string.IsNullOrEmpty(x)))
                                serv.Search(name);

                            break;

                        case '3':
                            foreach (var c in serv.CompaniesWithMatches)
                                Console.WriteLine(c.ToString());

                            break;

                        case '4':
                            serv.RemoveInvalidReplies();

                            break;

                        case '5':
                            var json = serv.AllResultsToJson();
                            File.WriteAllText($"out_{DateTime.Now.Ticks}.json", json);

                            break;


                        case '6':
                            Console.WriteLine("Good bye!");

                            return;

                    }

                    Console.WriteLine();
                    Console.WriteLine();

                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( "ERROR:"+ ex.Message );
            }




        }



    }
}
